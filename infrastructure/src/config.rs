#[derive(Default)]
pub struct Config {
    ops_api_url: String,
}

pub trait ConfigProvider {
    fn get_config() -> Config;
}

pub struct ConfigRepository;

impl ConfigProvider for ConfigRepository {
    fn get_config() -> Config {
        Config::default()
    }
}
