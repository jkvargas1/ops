//
// Created by vargasj on 07/02/2021.
//

#include <oswindow.h>
#include <GL/glew.h>
#include <backends/imgui_impl_sdl.h>

#define WINDOW_HEIGHT_MIN_SIZE 768
#define WINDOW_WIDTH_MIN_SIZE 1024

namespace Ops {
    OsWindow::OsWindow(const char *window_name) {
        this->window_flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI;
        this->window = SDL_CreateWindow(window_name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH_MIN_SIZE, WINDOW_HEIGHT_MIN_SIZE, this->window_flags);
        this->context = SDL_GL_CreateContext(this->window);
        SDL_GL_MakeCurrent(this->window, this->context);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_STENCIL_TEST);

        SDL_SetWindowMinimumSize(this->window, WINDOW_WIDTH_MIN_SIZE, WINDOW_HEIGHT_MIN_SIZE);
        SDL_MaximizeWindow(this->window);
    }

    OsWindow::~OsWindow() noexcept {
        SDL_GL_DeleteContext(this->context);
        SDL_DestroyWindow(this->window);
    }

    inline void OsWindow::swap_buffers() noexcept {
        SDL_GL_SwapWindow(this->window);
    }

    inline void OsWindow::init_graphic_api() noexcept {
        ImGui_ImplSDL2_InitForOpenGL(this->window, &this->context);
    }

    ImVec2 OsWindow::get_framebuffer_size() noexcept {
        int x, y;
        SDL_GetWindowSize(this->window, &x, &y);
        return ImVec2((float)x, (float)y);
    }

    void OsWindow::on_event(SDL_Event &event) {
    }

    ImVec2 OsWindow::get_window_position() noexcept {
        int x, y;
        SDL_GetWindowPosition(this->window, &x, &y);
        return ImVec2((float)x, (float)y);
    }

    inline void OsWindow::new_frame() noexcept {
        ImGui_ImplSDL2_NewFrame(this->window);
    }
}