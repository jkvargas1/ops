//
// Created by vargasj on 06/02/2021.
//

#include <assignedscheduleditem.h>
#include <gantt.h>
#include <implot.h>
#include <implot_internal.h>

#define ASPECT_TO_BE_USED 0.01

namespace Ops {
    Gantt::Gantt(Engine* engine, ITimeService* time_service, std::string name, bool visible)
    : UiView(engine, name, visible), time_service{ time_service }, ops_period{ time_service } {
        period_begin = static_cast<f64>(duration_cast<seconds>(ops_period.begin().time_since_epoch()).count());
        period_end = static_cast<f64>(duration_cast<seconds>(ops_period.end().time_since_epoch()).count());

        ImPlot::CreateContext();
    }

    void Gantt::update(f32 delta) {
        ImVec2 canvas_size = ImGui::GetContentRegionAvail();
        ImPlot::SetNextPlotLimitsX(period_begin, period_end);

        static const u8* labels[] = { "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10" };
        static const f64 positions[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        auto initial_scheduled_item = ops_period.begin() + hours{ 3 };
        auto end_scheduled_item = ops_period.begin() + hours(6);

        auto scheduled_item = AssignedScheduledItem{ initial_scheduled_item, end_scheduled_item, 2, AssignedScheduledItemType::Actual };

        ImPlot::SetNextPlotTicksY(positions, 10, labels);
        if (ImPlot::BeginPlot("", nullptr, nullptr, canvas_size, 0, ImPlotAxisFlags_Time | ImPlotAxisFlags_Lock)) {
            set_y_axis_aspect(ASPECT_TO_BE_USED);
            scheduled_item.draw();

            ImPlot::EndPlot();
        }
    }

    Gantt::~Gantt() noexcept {
        ImPlot::DestroyContext();
    }

    void Gantt::set_y_axis_aspect(f64 aspect) const noexcept {
        ImPlotContext& gp = *GImPlot;
        auto* plot = gp.CurrentPlot;
        plot->YAxis[0].SetAspect(aspect);
    }
} // namespace Ops