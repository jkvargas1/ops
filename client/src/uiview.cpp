//
// Created by vargasj on 11/02/2021.
//

#include <engine.h>
#include <uiview.h>
#include <utility>

namespace Ops {
    UiView::UiView(Engine* engine, std::string name, bool visible)
    : engine{ engine }, name{ std::move(name) },
      visible{ visible } {
    }

    std::string UiView::get_name() const noexcept {
        return this->name;
    }
} // namespace Ops
