//
// Created by vargasj on 19/02/2021.
//

#include <opsperiod.h>

#define DAYS_BACK 1
#define DAYS_FORWARD 2

namespace Ops {
    OpsPeriod::OpsPeriod(ITimeService* time_service) noexcept {
        auto current_date_time = time_service->get_current_date_time();
        auto current_date = floor<days>(current_date_time);

        starting_from = current_date - days{ DAYS_BACK };
        ending_at = current_date + days{ DAYS_FORWARD };
    }

    inline tpoint OpsPeriod::begin() noexcept {
        return starting_from;
    }

    inline tpoint OpsPeriod::end() noexcept {
        return ending_at;
    }
} // namespace Ops