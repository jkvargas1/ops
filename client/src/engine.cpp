//
// Created by vargasj on 10/02/2021.
//

#include <GL/glew.h>
#include <backends/imgui_impl_opengl3.h>
#include <backends/imgui_impl_sdl.h>
#include <engine.h>
#include <gantt.h>
#include <uiview.h>

#define OPEN_GLSL_VERSION "#version 330"
#define FONT_TO_USE "DroidSansMono.ttf"
#define FONT_SIZE 13.0f

namespace Ops {
    Engine::~Engine() {
        for (auto& view : views) {
            delete (view);
        }

        ImGui_ImplOpenGL3_Shutdown();
        ImGui::DestroyContext(imgui_context);
    }

    Engine::Engine(OsWindow* window, ITimeService* time_service)
    : window{ window }, time_service{ time_service } {
        // get window size
        this->frame_buffer_size = this->window->get_framebuffer_size();

        // set vsync on startup
        SDL_GL_SetSwapInterval(1);

        IMGUI_CHECKVERSION();
        this->imgui_context = ImGui::CreateContext();

        ImGuiIO& io = ImGui::GetIO();
        io.Fonts->AddFontDefault();
        io.Fonts->AddFontFromFileTTF(FONT_TO_USE, FONT_SIZE);

        io.ConfigFlags |= ImGuiConfigFlags_DockingEnable | ImGuiConfigFlags_NavEnableKeyboard;
        io.ConfigDockingNoSplit = true;
        io.FontAllowUserScaling = true;

        ImGuiStyle& style = ImGui::GetStyle();
        style.WindowMenuButtonPosition = ImGuiDir_Right;

        ImGui_ImplOpenGL3_Init(OPEN_GLSL_VERSION);
        this->window->init_graphic_api();

        ImGui::StyleColorsDark();

        this->views.push_back(new Gantt(this, this->time_service, "Gantt", true));
        //this->views.push_back(new FlightList(this, "Flight List", true));
    }

    void Engine::update(float delta) {
        ImGui_ImplOpenGL3_NewFrame();
        this->window->new_frame();
        ImGui::NewFrame();

        ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
        ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->Pos);
        ImGui::SetNextWindowSize(viewport->Size);
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        ImGui::Begin("DockSpaceWnd", 0, window_flags);
        ImGui::PopStyleVar(3);

        // DockSpace
        if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_DockingEnable) {
            ImGuiID dockspace_id = ImGui::GetID("DockSpace");
            ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), ImGuiDockNodeFlags_None);
        }

        ImGui::End();

        this->update_views(delta);

        ImGui::Render();
    }

    void Engine::render() {
        this->frame_buffer_size = this->window->get_framebuffer_size();

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        auto* draw_data = ImGui::GetDrawData();

        if (draw_data != nullptr) {
            ImGui_ImplOpenGL3_RenderDrawData(draw_data);

            if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
                ImGui::UpdatePlatformWindows();
                ImGui::RenderPlatformWindowsDefault();
            }
        }

        this->window->swap_buffers();
    }

    void Engine::update_views(float delta) {
        for (auto& view : views) {
            if (view->visible) {
                ImGuiViewport* viewport = ImGui::GetMainViewport();
                ImGui::SetNextWindowPos(viewport->Pos);
                ImGui::SetNextWindowSize(viewport->Size);
                //ImGui::SetNextWindowSizeConstraints(ImVec2(80, 80), this->frame_buffer_size * 2);
                if (ImGui::Begin(view->get_name().c_str(), &view->visible))
                    view->update(delta);
                ImGui::End();
            }
        }
    }
    
    void Engine::on_event(SDL_Event event) {
        ImGui_ImplSDL2_ProcessEvent(&event);
    }

} // namespace Ops