//
// Created by vargasj on 21/08/2021.
//

#include <assignedscheduleditem.h>
#include <implot.h>

namespace Ops {
    AssignedScheduledItem::AssignedScheduledItem(const tpoint& start, const tpoint& end, aircraft_index index, const AssignedScheduledItemType type) noexcept {
        this->start = static_cast<f64>(duration_cast<seconds>(start.time_since_epoch()).count());
        this->end = static_cast<f64>(duration_cast<seconds>(end.time_since_epoch()).count());

        switch (type) {
        case Scheduled:
            this->index = index + 0.5;
            this->index_end = index + 1;
            break;
        case Actual:
            this->index = index;
            this->index_end = index + 0.5;
            break;
        }
    }

    void AssignedScheduledItem::draw() noexcept {
        auto* draw_list = ImPlot::GetPlotDrawList();

        static ImVec4 bearcol = ImVec4(0.853f, 0.050f, 0.310f, 1.000f);

        auto color = ImGui::GetColorU32(bearcol);

        auto initial_point = ImPlot::PlotToPixels(this->start, this->index);
        auto end_point = ImPlot::PlotToPixels(this->end, this->index_end);

        draw_list->AddRectFilled(initial_point, end_point, color);
    }
} // namespace Ops
