//
// Created by vargasj on 19/02/2021.
//

#include <timeservice.h>

namespace Ops {
    inline tpoint TimeService::get_current_date_time() noexcept {
        auto current_time = system_clock::now();
        return floor<days>(current_time);
    }
} // namespace Ops