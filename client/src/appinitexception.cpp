//
// Created by vargasj on 05/04/2021.
//

#include <appinitexception.h>

#define ERROR_MESSAGE "Error when trying to initialize GLEW and or SDL"

namespace Ops {
    const char * AppInitException::what() const noexcept {
        return ERROR_MESSAGE;
    }
}