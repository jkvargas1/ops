//
// Created by vargasj on 13/02/2021.
//

#include <flightlist.h>
#include <engine.h>

namespace Ops {
    FlightList::FlightList(Engine* engine, std::string name, bool visible)
    : UiView(engine, name, visible) {
    }

    void FlightList::update(float delta) {
        ImGui::Text("Flight List");
    }
}