//
// Created by vargasj on 05/04/2021.
//

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <app.h>
#include <appinitexception.h>

namespace Ops {
    void App::run_loop() noexcept {
        SDL_Event event;
        do {
            if (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    break;
                }

                engine->on_event(event);
            }

            engine->update(10);

            engine->render();
        } while (true);
    }

    App::~App() {
        SDL_Quit();
    }

    App::App(ITimeService* time_service) {
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO) < 0) {
            throw AppInitException{};
        }

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // double buffering

        os_window = std::make_unique<OsWindow>("ops client");

        if (glewInit() != GLEW_OK) {
            throw AppInitException{};
        }

        engine = std::make_unique<Engine>(os_window.get(), time_service);
    }
} // namespace Ops