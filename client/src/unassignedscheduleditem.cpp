//
// Created by vargasj on 21/08/2021.
//

#include <implot.h>
#include <implot_internal.h>
#include <unassignedscheduleditem.h>

namespace Ops {
    ScheduledItem::ScheduledItem(const tpoint& start, const tpoint& end, const aircraft_index index) noexcept {
        this->start = static_cast<f64>(duration_cast<seconds>(start.time_since_epoch()).count());
        this->end = static_cast<f64>(duration_cast<seconds>(end.time_since_epoch()).count());
        this->index = index;
    }

    void ScheduledItem::draw() noexcept {
        auto* draw_list = ImPlot::GetPlotDrawList();

        static ImVec4 bearcol = ImVec4(0.853f, 0.050f, 0.310f, 1.000f);

        //        ImPlot::FitPoint(ImPlotPoint(this->start, 10.0));
        //        ImPlot::FitPoint(ImPlotPoint(5.0, 20.0));

        auto color = ImGui::GetColorU32(bearcol);

        auto initial_point = ImPlot::PlotToPixels(this->start, index);
        auto end_point = ImPlot::PlotToPixels(this->end, index + 1);

        draw_list->AddRectFilled(initial_point, end_point, color);
    }
} // namespace Ops