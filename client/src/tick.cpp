//
// Created by vargasj on 28/02/2021.
//

#include <tick.h>
#include <sstream>

namespace Ops {
    Tick::Tick(boost::gregorian::date date, float pixel_position) noexcept {
        this->label = boost::gregorian::to_simple_string(date).c_str();
        this->pixel_pos = pixel_position;
        this->text_size = ImVec2{ 100.0f, 100.f };
    }

    inline string Tick::convert_int_to_hour(int hour) const {
        std::ostringstream oss;
        oss << std::setfill('0') << std::setw(2) << hour << ":00";
        return oss.str();
    }

    const char* Tick::get_label() const noexcept {
        return this->label;
    }

    Tick::Tick(const char* label, float pixel_position) noexcept {
        this->label = label;
        this->pixel_pos = pixel_position;
        this->text_size = ImVec2{ 100.0f, 100.f };
    }

    ImVec2 Tick::get_text_size() const noexcept {
        return this->text_size;
    }

    bool Tick::intersects(ImVec2 region) const noexcept {
        return region.x < pixel_pos && region.y > pixel_pos;
    }
} // namespace Ops