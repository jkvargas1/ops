#include <timeservice.h>
#include <app.h>

int main(int, char**) {
    auto time_service = Ops::TimeService{};
    auto app = Ops::App{&time_service};

    app.run_loop();

    return 0;
}