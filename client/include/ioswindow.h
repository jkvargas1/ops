//
// Created by vargasj on 07/02/2021.
//

#ifndef OPS_IOSWINDOW_H
#define OPS_IOSWINDOW_H

#include <imgui.h>
#include <SDL2/SDL.h>

namespace Ops {
    class IOsWindow {
    public:
        virtual ~IOsWindow() noexcept = default;
        virtual void swap_buffers() noexcept = 0;
        virtual ImVec2 get_framebuffer_size() noexcept = 0;
        virtual ImVec2 get_window_position() noexcept = 0;
        virtual void new_frame() noexcept = 0;
        virtual void init_graphic_api() noexcept = 0;
        virtual void on_event(SDL_Event& event) = 0;
    };
}

#endif //OPS_IOSWINDOW_H
