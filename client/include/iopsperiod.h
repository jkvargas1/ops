//
// Created by vargasj on 08/08/2021.
//

#ifndef OPS_IOPSPERIOD_H
#define OPS_IOPSPERIOD_H

#include <opsapi.h>
#include <chrono>
#include <base.h>

namespace Ops {
    class IOpsPeriod : public OpsApi {
    public:
        virtual tpoint begin() noexcept = 0;
        virtual tpoint end() noexcept = 0;
    };
}

#endif // OPS_IOPSPERIOD_H
