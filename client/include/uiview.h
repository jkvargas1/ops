//
// Created by vargasj on 11/02/2021.
//

#ifndef OPS_UIVIEW_H
#define OPS_UIVIEW_H

#include <string>
#include <opsapi.h>

namespace Ops {
    class Engine;

    class UiView : public OpsApi {
    public:
        explicit UiView(Engine* engine, std::string  name = "", bool visible = true);
        virtual void update(float delta) = 0;
        [[nodiscard]] std::string get_name() const noexcept;
        bool visible;
    protected:
        Engine* engine;
        std::string name;
    };
}

#endif //OPS_UIVIEW_H
