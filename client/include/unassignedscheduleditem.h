//
// Created by vargasj on 21/08/2021.
//

#ifndef OPS_UNASSIGNEDSCHEDULEDITEM_H
#define OPS_UNASSIGNEDSCHEDULEDITEM_H

#include <idrawable.h>
#include <base.h>

namespace Ops {
    class ScheduledItem : public IDrawable {
    public:
        explicit ScheduledItem(const tpoint& start, const tpoint& end, aircraft_index index) noexcept;

        void draw() noexcept override;

    protected:
        f64 start;
        f64 end;
        aircraft_index index;
    };
}

#endif // OPS_UNASSIGNEDSCHEDULEDITEM_H
