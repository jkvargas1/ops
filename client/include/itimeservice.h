//
// Created by vargasj on 19/02/2021.
//

#ifndef OPS_ITIMESERVICE_H
#define OPS_ITIMESERVICE_H

#include <opsapi.h>
#include <chrono>
#include <base.h>

namespace Ops {
    class ITimeService : public OpsApi {
    public:
        virtual tpoint get_current_date_time() noexcept = 0;
    };
} // namespace Ops

#endif //OPS_ITIMESERVICE_H
