//
// Created by vargasj on 13/02/2021.
//

#ifndef OPS_FLIGHTLIST_H
#define OPS_FLIGHTLIST_H

#include <uiview.h>

namespace Ops {
    class Engine;

    class FlightList : public UiView {
    public:
        explicit FlightList(Engine* engine, std::string  name = "", bool visible = true);
        void update(float delta) override;
    };
}


#endif //OPS_FLIGHTLIST_H
