//
// Created by vargasj on 19/02/2021.
//

#ifndef OPS_OPSPERIOD_H
#define OPS_OPSPERIOD_H

#include <opsapi.h>
#include <iopsperiod.h>
#include <itimeservice.h>
#include <chrono>

namespace Ops {
    class OpsPeriod : public IOpsPeriod {
    public:
        explicit OpsPeriod(ITimeService* time_service) noexcept;
        [[nodiscard]] tpoint begin() noexcept override;
        [[nodiscard]] tpoint end() noexcept override;
    protected:
        tpoint starting_from;
        tpoint ending_at;
    };
} // namespace Ops

#endif // OPS_OPSPERIOD_H
