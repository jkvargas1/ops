//
// Created by vargasj on 04/02/2021.
//

#ifndef OPS_OPSAPI_H
#define OPS_OPSAPI_H

#include <imgui.h>
#include <cstddef>

namespace Ops {
    class OpsApi {
    protected:
        // disallow creation on the stack
        OpsApi() noexcept = default;

        virtual ~OpsApi() = default;

    public:
        // disallow copy and assignment
        OpsApi(OpsApi const&) = delete;

        OpsApi(OpsApi&&) = delete;

        OpsApi& operator=(OpsApi const&) = delete;

        OpsApi& operator=(OpsApi&&) = delete;

        static void* operator new[](size_t) = delete;

        //static void operator delete(void *) = delete;

        static void operator delete[](void*) = delete;
    };
} // namespace Ops

#endif //OPS_OPSAPI_H
