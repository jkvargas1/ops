//
// Created by vargasj on 07/02/2021.
//

#ifndef OPS_ENGINE_H
#define OPS_ENGINE_H

#include <opsapi.h>
#include <oswindow.h>
#include <imgui.h>
#include <vector>

namespace Ops {
    class UiView;
    class ITimeService;

    class Engine : public OpsApi {
    public:
        explicit Engine(OsWindow* window, ITimeService* time_service);
        ~Engine() override;

        void update(float delta);
        void render();

        void on_event(SDL_Event event);
    protected:
        void update_views(float delta);

        ITimeService* time_service;
        ImGuiContext* imgui_context;
        OsWindow* window;
        ImVec2 frame_buffer_size;
        std::vector<UiView*> views;
    };
}

#endif //OPS_ENGINE_H
