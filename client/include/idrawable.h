//
// Created by vargasj on 21/08/2021.
//

#include <opsapi.h>

#ifndef OPS_IDRAWABLE_H
#define OPS_IDRAWABLE_H

namespace Ops {
    class IDrawable : public OpsApi {
    public:
        virtual void draw() noexcept = 0;
    };
}

#endif // OPS_IDRAWABLE_H
