//
// Created by vargasj on 12/08/2021.
//

#ifndef OPS_BASE_H
#define OPS_BASE_H

#include <chrono>

using namespace std::chrono;

namespace Ops {
    typedef float f32;
    typedef double f64;
    typedef unsigned int u32;
    typedef char u8;
    typedef f64 aircraft_index;

    typedef std::chrono::time_point<system_clock, seconds> tpoint;
}

#endif // OPS_BASE_H
