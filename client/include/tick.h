//
// Created by vargasj on 28/02/2021.
//

#ifndef OPS_TICK_H
#define OPS_TICK_H

#include <boost/date_time/gregorian/gregorian.hpp>
#include <string>
#include <imgui.h>

using namespace std;

namespace Ops {
    class Tick {
    public:
        explicit Tick(const char* label, float pixel_position) noexcept;
        explicit Tick(boost::gregorian::date date, float pixel_position) noexcept;
        [[nodiscard]] const char* get_label() const noexcept;
        [[nodiscard]] ImVec2 get_text_size() const noexcept;
        [[nodiscard]] bool intersects(ImVec2 region) const noexcept;

    protected:
        [[nodiscard]] string convert_int_to_hour(int hour) const;
        const char* label;
        float pixel_pos;
        ImVec2 text_size;
    };
}

#endif //OPS_TICK_H
