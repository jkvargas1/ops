//
// Created by vargasj on 05/04/2021.
//

#ifndef OPS_APP_H
#define OPS_APP_H

#include <opsapi.h>
#include <engine.h>
#include <itimeservice.h>
#include <memory>

using namespace std;

namespace Ops {
    class App : public OpsApi {
    public:
        explicit App(ITimeService* time_service);
        void run_loop() noexcept;
        ~App() override;

    protected:
        unique_ptr<OsWindow> os_window;
        unique_ptr<Engine> engine;
    };
}

#endif //OPS_APP_H
