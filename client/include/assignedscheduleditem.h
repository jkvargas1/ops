//
// Created by vargasj on 21/08/2021.
//

#ifndef OPS_ASSIGNEDSCHEDULEDITEM_H
#define OPS_ASSIGNEDSCHEDULEDITEM_H

#include <idrawable.h>
#include <base.h>

namespace Ops {
    enum AssignedScheduledItemType {
        Scheduled,
        Actual,
    };

    class AssignedScheduledItem : public IDrawable {
    public:
        explicit AssignedScheduledItem(const tpoint& start, const tpoint& end, aircraft_index index, AssignedScheduledItemType type) noexcept;

        void draw() noexcept override;

    protected:
        aircraft_index index;
        f64 start;
        f64 end;
        aircraft_index index_end;
    };
} // namespace Ops

#endif // OPS_ASSIGNEDSCHEDULEDITEM_H
