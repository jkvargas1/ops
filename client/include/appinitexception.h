//
// Created by vargasj on 05/04/2021.
//

#ifndef OPS_APPINITEXCEPTION_H
#define OPS_APPINITEXCEPTION_H

#include <opsapi.h>
#include <exception>

namespace Ops {
    class AppInitException : virtual public std::exception, public OpsApi {
        [[nodiscard]] const char* what() const noexcept override;
    };
} // namespace Ops

#endif // OPS_APPINITEXCEPTION_H
