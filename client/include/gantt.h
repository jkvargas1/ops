//
// Created by vargasj on 06/02/2021.
//

#ifndef OPS_GANTT_H
#define OPS_GANTT_H

#include <imgui.h>
#include <uiview.h>
#include <string>
#include <opsperiod.h>

namespace Ops {
    class Engine;
    class ITimeService;

    class Gantt : public UiView {
    public:
        explicit Gantt(Engine* engine, ITimeService* time_service, std::string name = "", bool visible = true);
        ~Gantt() noexcept override;
        void update(f32 delta) override;

    protected:
        void set_y_axis_aspect(f64 aspect) const noexcept;

        f64 period_begin;
        f64 period_end;

        ITimeService* time_service;
        OpsPeriod ops_period;
    };
} // namespace Ops

#endif //OPS_GANTT_H
