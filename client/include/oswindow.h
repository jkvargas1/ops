//
// Created by vargasj on 07/02/2021.
//

#ifndef OPS_OSWINDOW_H
#define OPS_OSWINDOW_H

#include <opsapi.h>
#include <ioswindow.h>
#include <SDL2/SDL_video.h>
#include <imgui.h>

namespace Ops {
    class OsWindow : public IOsWindow, public OpsApi {
    public:
        explicit OsWindow(const char* window_name);
        void swap_buffers() noexcept override;
        void init_graphic_api() noexcept override;
        ImVec2 get_framebuffer_size() noexcept override;
        ImVec2 get_window_position() noexcept override;
        void on_event(SDL_Event& event) override;
        void new_frame() noexcept override;
        ~OsWindow() noexcept override;
    protected:
        SDL_Window* window;
        SDL_GLContext context;
        Uint32 window_flags;
    };
}

#endif //OPS_OSWINDOW_H
