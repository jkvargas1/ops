//
// Created by vargasj on 19/02/2021.
//

#ifndef OPS_TIMESERVICE_H
#define OPS_TIMESERVICE_H

#include <itimeservice.h>

namespace Ops {
    class TimeService : public ITimeService {
        [[nodiscard]] tpoint get_current_date_time() noexcept override;
    };
} // namespace Ops

#endif //OPS_TIMESERVICE_H
